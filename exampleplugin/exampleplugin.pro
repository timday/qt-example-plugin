TEMPLATE = lib

CONFIG += plugin
CONFIG += c++11

CONFIG += release

QT += core gui quick qml

TARGET = exampleplugin

SOURCES += ExamplePlugin.cpp RandomText.cpp
HEADERS += RandomText.h

OBJECTS_DIR = build/obj
MOC_DIR = build/moc
RCC_DIR = build/qrc
UI_DIR = build/ui

unix:!mac{
  DEFINES += LINUXQUIRKS
  DESTDIR = imports/ExamplePlugin
  QMAKE_CXXFLAGS += -std=c++11
}

macx {
  DEFINES += MACXQUIRKS
  DESTDIR = imports/ExamplePlugin
  QMAKE_CXXFLAGS += -std=c++11
}

ios {
  DEFINES += IOSQUIRKS
  DESTDIR = imports-static/ExamplePlugin
  CONFIG += static  # Probably redundant
  QMAKE_MOC_OPTIONS += -Muri=com.example  # Without this, get an error message about static plugin for module "QtQuick" with name "ExamplePlugin" has no metadata URI
}

cache()
