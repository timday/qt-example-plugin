#include "RandomText.h"

RandomText::RandomText() {

  changeText();

  QObject::connect(&_ticker,SIGNAL(timeout()),this,SLOT(changeText()));
  _ticker.setSingleShot(false);
  _ticker.start(1000.0);
}

RandomText::~RandomText() {
}

QString RandomText::text() const {
  return _text;
}

void RandomText::changeText() {
  _text.clear();
  const int length=3+(qrand()>>8)%5;
  for (int i=0;i<length;++i) {
    _text+='a'+(qrand()>>8)%(1+'z'-'a');
  }

  emit textChanged(text());
}
