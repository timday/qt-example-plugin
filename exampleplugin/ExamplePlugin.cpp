#include <iostream>

#include <QQmlExtensionPlugin>
#include <QtQml>

#include "RandomText.h"

class ExamplePlugin : public QQmlExtensionPlugin {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "com.example.exampleplugin")

public:

  ExamplePlugin() {
    std::cerr << "ExamplePlugin constructed" << std::endl;
  }
  ~ExamplePlugin() {
    std::cerr << "ExamplePlugin destroyed" << std::endl;
  }

  void registerTypes(const char* url) {
    std::cerr << "ExamplePlugin::registerTypes(" << '"' << url << '"' << ") invoked" << std::endl;
    qmlRegisterType<RandomText>(url,1,0,"RandomText");
  }
};

// Important!  This includes some metadata which avoids a frustrating and opaque
// "Plugin verification data mismatch" error when attempting to use the plugin.
// And the include is needed for qmake to trigger the generation of the file!
#include "ExamplePlugin.moc"
